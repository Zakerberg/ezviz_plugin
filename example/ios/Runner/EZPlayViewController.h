//
//  EZPlayViewController.h
//  Runner
//
//  Created by 柏柏柏柏柏 on 2021/4/6.
//

#import <UIKit/UIKit.h>

@class EZHCNetDeviceInfo;

@interface EZPlayViewController : UIViewController

@property (nonatomic,strong) EZHCNetDeviceInfo *deviceInfo;

// 对讲相关的deviceInfo
//@property (nonatomic,strong) EZDeviceInfo *deviceInfotalk;


@property (nonatomic,assign) NSInteger cameraNo;//通道号


@end
