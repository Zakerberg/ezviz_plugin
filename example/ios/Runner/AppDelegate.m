#import "AppDelegate.h"
#import "GeneratedPluginRegistrant.h"
#import <Flutter/Flutter.h>
#import "FlutterPlatformViewFactory.h"
#import <EZOpenSDKFramework/EZOpenSDKFramework.h>

@implementation AppDelegate:FlutterAppDelegate

- (BOOL)application:(UIApplication *)application
    didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
  [GeneratedPluginRegistrant registerWithRegistry:self];
  // Override point for customization after application launch.

    
    /* Swift:
     let registrar:FlutterPluginRegistrar = self.registrar(forPlugin: "plugins.flutter.io/custom_platform_view_plugin")!
     let factory = MyFlutterViewFactory(messenger: registrar.messenger())
     registrar.register(factory, withId: "plugins.flutter.io/custom_platform_view")
     return super.application(application, didFinishLaunchingWithOptions: launchOptions)
     */

//    NSObject <FlutterPluginRegistrar> *registar = [self registrarForPlugin:@"EzvizPlugin"];
//    FlutterPlatformViewFactory *factory =[[FlutterPlatformViewFactory alloc]initWithMessager: registar.messenger];
//    [registar registerViewFactory:factory withId:@"ezviz_flutter_plugin_ios_view"];
//    
    [EZOpenSDK setDebugLogEnable:YES];
    [EZOpenSDK initLibWithAppKey:@"877fa59eea614e0e8ee2bd46da610d04"];
    [EZHCNetDeviceSDK initSDK];
    [EZOpenSDK enableP2P:YES];
    
    return [super application:application didFinishLaunchingWithOptions:launchOptions];
}

@end
