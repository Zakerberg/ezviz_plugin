//
//  EZPlayViewController.m
//  Runner
//
//  Created by 柏柏柏柏柏 on 2021/4/6.
//

#import "EZPlayViewController.h"
#import <UIKit/UIKit.h>
#import <EZOpenSDKFramework/EZOpenSDKFramework.h>

@interface EZPlayViewController()<EZPlayerDelegate>

@property (nonatomic, strong) UIButton *leftBtn;
@property (nonatomic, strong) UIButton *rightBtn;
@property (nonatomic, strong) UIButton *topBtn;
@property (nonatomic, strong) UIButton *downBtn;
@property (nonatomic, strong) EZPlayer *player;
@property (nonatomic, strong) UIView *playerView;
@property (nonatomic, strong) UIButton *bgBtn;

@end


@implementation EZPlayViewController


- (void)viewDidLoad
{
  //  [super viewDidLoad];
    [self startRealPlay];
}

- (void) viewDidDisappear:(BOOL)animated
{
  //  [super viewDidDisappear:animated];
    [self.player stopRealPlay];
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

#pragma mark - 按钮的点击事件action
- (void)ptzStart:(id)sender
{
    EZPTZCommandType command;
    NSString *imageName = nil;
    if(sender == self.leftBtn)
    {
        command = EZPTZCommandType_LEFT;
        imageName = @"ptz_left_sel";
    }
    else if (sender == self.downBtn)
    {
        command = EZPTZCommandType_DOWN;
        imageName = @"ptz_bottom_sel";
    }
    else if (sender == self.rightBtn)
    {
        command = EZPTZCommandType_RIGHT;
        imageName = @"ptz_right_sel";
    }
    else {
        command = EZPTZCommandType_UP;
        imageName = @"ptz_up_sel";
    }
    
    [self.bgBtn setImage:[UIImage imageNamed:imageName] forState:UIControlStateDisabled];
    [self ptzWithCommand:command action:EZPTZActionType_START];
}

//暂停
- (void)ptzStop:(id)sender
{
    EZPTZCommandType command;
    if(sender == self.leftBtn)
    {
        command = EZPTZCommandType_LEFT;
    }
    else if (sender == self.downBtn)
    {
        command = EZPTZCommandType_DOWN;
    }
    else if (sender == self.rightBtn)
    {
        command = EZPTZCommandType_RIGHT;
    }
    else {
        command = EZPTZCommandType_UP;
    }
    [self.bgBtn setImage:[UIImage imageNamed:@"ptz_bg"] forState:UIControlStateDisabled];
    [self ptzWithCommand:command action:EZPTZActionType_STOP];
}

#pragma mark - player delegate

- (void)player:(EZPlayer *)player didPlayFailed:(NSError *)error
{
    NSLog(@"local player error :%@",error);
}

- (void)player:(EZPlayer *)player didReceivedMessage:(NSInteger)messageCode
{
    

}

#pragma mark - support

- (void) startRealPlay
{
    self.player = [EZPlayer createPlayerWithUserId:self.deviceInfo.userId cameraNo:self.cameraNo streamType:1];
    
    [self.player setPlayerView:self.playerView];
    self.player.delegate = self;
    
    [self.player startRealPlay];
}

- (void) ptzWithCommand:(EZPTZCommandType) command action:(EZPTZActionType) action
{
    [EZHCNetDeviceSDK ptzControlWithUserId:self.deviceInfo.userId channelNo:self.cameraNo command:command action:action];
}









@end
