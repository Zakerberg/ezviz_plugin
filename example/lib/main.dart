import 'package:ezviz_plugin_example/test_flutter_demo.dart';
import 'package:flutter/material.dart';
import 'package:ezviz_plugin/ezviz_plugin.dart';
import 'package:ezviz_plugin/ezviz_camera_view.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  var testFlutterDemo;
  double height = 200;
  GlobalKey playerKey = GlobalKey();

  @override
  void initState() {
    super.initState();
  }

  Widget buildFlatButton(Function onPressed, String text) {
    return FlatButton(
      onPressed: onPressed,
      child: Text(text),
    );
  }

  @override
  Widget build(BuildContext context) {
    TestView testView = TestView(onCreated: onTestViewCreated);

    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Plugin example app'),
        ),
        body: ListView(
          children: [
            // Container(height: 200, child: testView),
            EzvizCameraView(
              height: height,
              // key: playerKey,
              accessToken: null,
              deviceSerial: null,
            ),
            FlatButton(
              onPressed: () {
                setState(() {
                  height = height == 200 ? 300 : 200;
                });
              },
              child: Text('切换视频尺寸'),
            ),
            FlatButton(
              onPressed: () async {
                EzvizPlugin.startToPlay(
                  accessToken: 'at.du1mmksdbac93lmlc7bdrj08d0j8k6ho-1ou0fgevkf-1kpvzlk-xhie1bdci',
                  deviceSerial: 'F03174481',
                );
              },
              child: Text('初始化'),
            ),
            GestureDetector(
              onTapDown: (_) {
                EzvizPlugin.setDirection(EzvizPluginConstants.DIRECTION_UP);
              },
              onTapUp: (_) {
                EzvizPlugin.setDirection(
                  EzvizPluginConstants.DIRECTION_UP,
                  true,
                );
              },
              child: Container(
                color: Colors.red,
                padding: EdgeInsets.symmetric(vertical: 10),
                child: Text('按住上移'),
              ),
            ),
            GestureDetector(
              onTapDown: (_) {
                EzvizPlugin.setDirection(EzvizPluginConstants.DIRECTION_DOWN);
              },
              onTapUp: (_) {
                EzvizPlugin.setDirection(
                  EzvizPluginConstants.DIRECTION_UP,
                  true,
                );
              },
              child: Container(
                color: Colors.blue,
                padding: EdgeInsets.symmetric(vertical: 10),
                child: Text('按住下移'),
              ),
            ),
            GestureDetector(
              onTapDown: (_) {
                EzvizPlugin.setDirection(EzvizPluginConstants.DIRECTION_LEFT);
              },
              onTapUp: (_) {
                EzvizPlugin.setDirection(
                  EzvizPluginConstants.DIRECTION_LEFT,
                  true,
                );
              },
              child: Container(
                color: Colors.red,
                padding: EdgeInsets.symmetric(vertical: 10),
                child: Text('按住左移'),
              ),
            ),
            GestureDetector(
              onTapDown: (_) {
                EzvizPlugin.setDirection(EzvizPluginConstants.DIRECTION_RIGHT);
              },
              onTapUp: (_) {
                EzvizPlugin.setDirection(
                  EzvizPluginConstants.DIRECTION_RIGHT,
                  true,
                );
              },
              child: Container(
                color: Colors.blue,
                padding: EdgeInsets.symmetric(vertical: 10),
                child: Text('按住右移'),
              ),
            ),
            buildFlatButton(() => EzvizPlugin.closeSound(), "关闭声音"),
            buildFlatButton(() => EzvizPlugin.openSound(), "开启声音"),
            // buildFlatButton(() => EzvizPlugin.enterFullScreen(), "全屏"),
            // buildFlatButton(() => EzvizPlugin.exitFullScreen(), "退出全屏"),
          ],
        ),
      ),
    );
  }

  void onTestViewCreated(testFlutterPluginDemo) {
    this.testFlutterDemo = testFlutterPluginDemo;
  }
}
