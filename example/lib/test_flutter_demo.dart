import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

// 成功的回调
typedef void TextViewCreateCallBack(TestFlutterPluginDemo controller);

class TestFlutterPluginDemo {
  MethodChannel _channel;
  TestFlutterPluginDemo.init(int id) {
    _channel = new MethodChannel('ezviz_plugin');
  }
}

class TestView extends StatefulWidget {
  final TextViewCreateCallBack onCreated;
  final String titleStr;

  const TestView({Key key, this.onCreated, this.titleStr})
      : super(key: key); //上面创建的回调

  @override
  _TestViewState createState() => _TestViewState();
}

class _TestViewState extends State<TestView> {
  @override
  Widget build(BuildContext context) {
    return Container(child: _loadNativeView());
  }

  _loadNativeView() {
    if (Platform.isAndroid) {
      return AndroidView(
        viewType: 'testView', // 视图标识符, 与原生一致
        onPlatformViewCreated: onplatformViewCreated,
        creationParams: <String, dynamic>{'titleStr': widget.titleStr},
        //  用来编码 creationParams 的形式，可选 [StandardMessageCodec], [JSONMessageCodec], [StringCodec], or [BinaryCodec]
        //  如果存在 creationParams，则该值不能为null
        creationParamsCodec: const StandardMessageCodec(),
      );
    } else if (Platform.isIOS) {
      return UiKitView(
        viewType: 'ezviz_flutter_plugin_ios_view', //! 视图标识符, 与原生一致
        onPlatformViewCreated: onplatformViewCreated,
        creationParams: <String, dynamic>{'titleStr': widget.titleStr},
        //  用来编码 creationParams 的形式，可选 [StandardMessageCodec], [JSONMessageCodec], [StringCodec], or [BinaryCodec]
        //  如果存在 creationParams，则该值不能为null
        creationParamsCodec: const StandardMessageCodec(),
      );
    } else {
      print('暂时不支持');
    }
  }

  Future<void> onplatformViewCreated(id) async {
    if (widget.onCreated == null) {
      return;
    }
    widget.onCreated(new TestFlutterPluginDemo.init(id));
  }
}
