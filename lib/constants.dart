class EzvizPluginConstants {
  static const int DIRECTION_UP = 0; // 上
  static const int DIRECTION_DOWN = 1; // 下
  static const int DIRECTION_LEFT = 2; // 左
  static const int DIRECTION_RIGHT = 3; // 右
  static const int MSG_REALPLAY_PLAY_SUCCESS = 102; // 播放成功
  static const int MSG_REALPLAY_PLAY_FAIL = 103; // 播放失败
  static const int MSG_WINDOW_SIZE_CHANGING = 1000; // 播放窗口尺寸改变
}
