import 'package:ezviz_plugin/enums.dart';

class EzvizEvent {
  EzvizStatus status = EzvizStatus.idle;
  EzvizEvent(this.status);
}
