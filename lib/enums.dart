enum EzvizStatus {
  idle, // 闲置状态
  playSuccess, // 播放成功
  playFail, // 播放失败
  sizeChanging, // 尺寸改变
}
