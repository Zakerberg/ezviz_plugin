import 'dart:async';
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'package:event_bus/event_bus.dart';
export 'package:ezviz_plugin/ezviz_camera_view.dart';
export 'package:ezviz_plugin/constants.dart';
export 'package:ezviz_plugin/enums.dart';
export 'package:ezviz_plugin/ezviz_event.dart';

class EzvizPlugin {
  static const MethodChannel _channel = const MethodChannel('ezviz_plugin');

  static Future<String> get platformVersion async {
    final String version = await _channel.invokeMethod('getPlatformVersion');
    return version;
  }

  static final EventBus eventBus = EventBus();

  /// 获取安卓的注册通道id
  static get getAndroidViewTypeId => 'ezviz_flutter_plugin_android_view';
  static get getiOSViewTypeId => 'ezviz_flutter_plugin_ios_view';

  /// 获取EventChannel的id
  static get getEventChannelId => 'ezviz_flutter_plugin_event_channel';

  /// 初始化SDK
  static Future<bool> init({
    String appKey,
  }) async {
    return await _channel.invokeMethod('init', {
      'appKey': appKey,
    });
  }

  /// 开始播放
  static Future<void> startToPlay({
    @required String accessToken,
    @required String deviceSerial,
    int cameraNo,
  }) async {
    await _channel.invokeMethod('startToPlay', {
      'accessToken': accessToken,
      'deviceSerial': deviceSerial,
      'cameraNo': cameraNo != null ? cameraNo.toString() : cameraNo,
    });
  }

  /// 停止播放
  static Future<bool> stopToPlay() async {
    return await _channel.invokeMethod('stopToPlay');
  }

  /// 打开声音
  static Future<void> openSound() async {
    await _channel.invokeMethod('openSound');
  }

  /// 关闭声音
  static Future<void> closeSound() async {
    await _channel.invokeMethod('closeSound');
  }

  /// 释放播放器资源
  static Future<void> release() async {
    await _channel.invokeMethod('release');
  }

  /// 移动播放器位置
  static Future<bool> setDirection(int direction, [bool stop = false]) async {
    return await _channel.invokeMethod('setDirection', {
      'direction': direction.toString(),
      'stop': stop,
    });
  }

  // /// 进入全屏
  // static Future<void> enterFullScreen() async {
  //   await _channel.invokeMethod('enterFullScreen');
  // }
  //
  // /// 退出全屏
  // static Future<void> exitFullScreen() async {
  //   await _channel.invokeMethod('exitFullScreen');
  // }
}
