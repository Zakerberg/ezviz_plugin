//
//  Factory.h
//  ezviz_plugin
//
//  Created by 柏柏柏柏柏 on 2021/4/6.
//

#import <Foundation/Foundation.h>
#import <Flutter/Flutter.h>

NS_ASSUME_NONNULL_BEGIN

@interface FlutterPlatformViewFactory : NSObject<FlutterPlatformViewFactory>

// 重写一个构造方法来接受Flutter 相关
-(instancetype) initWithMessager:(NSObject<FlutterBinaryMessenger>*) messager;

@end

NS_ASSUME_NONNULL_END
