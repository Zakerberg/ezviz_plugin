#import "EzvizPlugin.h"
#import "FlutterPlatformViewFactory.h"
#import <UIKit/UIKit.h>

@implementation EzvizPlugin
//+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
//  FlutterMethodChannel* channel = [FlutterMethodChannel
//      methodChannelWithName:@"ezviz_plugin"
//            binaryMessenger:[registrar messenger]];
//  EzvizPlugin* instance = [[EzvizPlugin alloc] init];
//  [registrar addMethodCallDelegate:instance channel:channel];
//}
//
//- (void)handleMethodCall:(FlutterMethodCall*)call result:(FlutterResult)result {
//  if ([@"getPlatformVersion" isEqualToString:call.method]) {
//    result([@"iOS " stringByAppendingString:[[UIDevice currentDevice] systemVersion]]);
//  } else {
//    result(FlutterMethodNotImplemented);
//  }
//}


+(void) registerWithRegistrar:(NSObject<FlutterPluginRegistrar> *)registrar {
    FlutterPlatformViewFactory * flutterPlatformViewFactory = [[FlutterPlatformViewFactory alloc] initWithMessager:registrar.messenger];
    //这里填写的id一定要和dart里面的ViewType一样, 这个参数传的id一样
    [registrar registerViewFactory:flutterPlatformViewFactory withId:@"ezviz_flutter_plugin_ios_view"];
}

@end
