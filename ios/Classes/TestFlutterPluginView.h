//
//  testView.h
//  ezviz_plugin
//
//  Created by 柏柏柏柏柏 on 2021/4/7.
//

#import <Foundation/Foundation.h>
#include <Flutter/Flutter.h>

NS_ASSUME_NONNULL_BEGIN

@interface TestFlutterPluginView : NSObject<FlutterPlatformView>

- (id)initWithFrame:(CGRect)frame
             viewId:(int64_t)viewId
               args:(id)args
           messager:(NSObject<FlutterBinaryMessenger>*)messenger;
@end

NS_ASSUME_NONNULL_END
