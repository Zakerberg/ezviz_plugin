#import <Flutter/Flutter.h>
#import <UIKit/UIKit.h>

@class EZHCNetDeviceInfo;

@interface EzvizPlugin : NSObject<FlutterPlugin>

@property (nonatomic,strong) EZHCNetDeviceInfo *deviceInfo;

// 对讲相关的deviceInfo
//@property (nonatomic,strong) EZDeviceInfo *deviceInfotalk;

@property (nonatomic,assign) NSInteger cameraNo;//通道号

@end
