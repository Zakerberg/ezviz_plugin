/* 
 * @ProjectName ezviz-openapi-android-demo
 * @Copyright null
 * 
 * @FileName Contants.java
 * @Description 这里对文件进行描述
 * 
 * @author chenxingyf1
 * @data 2015-5-12
 * 
 * @note 这里写本文件的详细功能描述和注释
 * @note 历史记录
 * 
 * @warning 这里写本文件的相关警告
 */
package com.example.ezviz_plugin.util;


public class Constants {
    public static final String APP_KEY = "877fa59eea614e0e8ee2bd46da610d04";
    public static final int CAMERA_NO = 1; // 默认是1
    public static final int DIRECTION_UP = 0; // 上
    public static final int DIRECTION_DOWN = 1; // 下
    public static final int DIRECTION_LEFT = 2; // 左
    public static final int DIRECTION_RIGHT = 3; // 右
}
