package com.example.ezviz_plugin.platformView;

import android.content.Context;
import android.view.SurfaceView;
import android.widget.RelativeLayout;

import io.flutter.Log;
import io.flutter.plugin.common.StandardMessageCodec;
import io.flutter.plugin.platform.PlatformView;
import io.flutter.plugin.platform.PlatformViewFactory;

public class RelativeLayoutPlatformFactory extends PlatformViewFactory {

    private RelativeLayoutPlatform relativeLayoutPlatform;

    public RelativeLayoutPlatformFactory() {
        super(StandardMessageCodec.INSTANCE);
    }

    @Override
    public PlatformView create(Context context, int i, Object o) {

        relativeLayoutPlatform = new RelativeLayoutPlatform(context);
        Log.e("TAG","开始创建一个RelativeLayoutPlatform");
        return relativeLayoutPlatform;
    }


    public RelativeLayout getRelativeLayout() {
        if (relativeLayoutPlatform != null)
            return relativeLayoutPlatform.getRelativeLayout();
        return null;
    }

    public SurfaceView getSurfaceView() {
        if (relativeLayoutPlatform != null)
            return relativeLayoutPlatform.getSurfaceView();
        return null;
    }

}
