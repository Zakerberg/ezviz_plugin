package com.example.ezviz_plugin.platformView;

import android.content.Context;
import android.view.SurfaceView;
import android.view.View;

import io.flutter.plugin.platform.PlatformView;

public class SurfaceViewPlatform implements PlatformView {

    private SurfaceView surfaceView;

    public SurfaceViewPlatform(Context context) {
        surfaceView = new SurfaceView(context);
    }

    @Override
    public View getView() {
        return surfaceView;
    }

    public SurfaceView getSurfaceView() {
        return surfaceView;
    }

    @Override
    public void dispose() {}
}
