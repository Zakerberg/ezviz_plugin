package com.example.ezviz_plugin.platformView;

import android.content.Context;
import android.graphics.Color;
import android.view.SurfaceView;
import android.view.View;
import android.widget.RelativeLayout;
import io.flutter.plugin.platform.PlatformView;

public class RelativeLayoutPlatform implements PlatformView {
    private RelativeLayout relativeLayout;
    private SurfaceView surfaceView;

    public RelativeLayoutPlatform(Context context) {
        relativeLayout = new RelativeLayout(context);
        relativeLayout.setBackgroundColor(Color.parseColor("#000000"));
        // 同时创建SurfaceView
        surfaceView = new SurfaceView(context);
    }

    @Override
    public View getView() {
        return relativeLayout;
    }

    public RelativeLayout getRelativeLayout() {
        return relativeLayout;
    }

    public SurfaceView getSurfaceView() {
        return surfaceView;
    }

    @Override
    public void dispose() {}
}
